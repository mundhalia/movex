# Movex

[![Latest Version on Packagist][ico-version]][link-packagist]


## Installation

Via Composer

``` bash
$ composer require mundhalia/movex
```

## Usage

- Publish the package config by running below command:

    `php artisan vendor:publish --provider="Mundhalia\Movex\MovexServiceProvider"`

- Now add like below in your .env file replace `<username>`, `<password>` and `<url>` with the correct value. Add your list of API's
```bash
MOVEX_USERNAME='<username>'
MOVEX_PASSWORD='<password>'
MOVEX_URL='https://<url>/m3api-rest/execute/'
MOVEX_API='{"itemToolboxInterface":"MMS200MI","productInterface":"PDS001MI","productStructureInterface":"PDS002MI"}'
```

- Add provider:

Open up `config/app.php`
```php
/*
* Package Service Providers...
*/
Mundhalia\Movex\MovexServiceProvider::class,
```
- Add Facade:
Open up `config/app.php`
```php
'aliases' => [
    ...
    'Movex' => Mundhalia\Movex\Facades\Movex::class,
]
```

## Examples:

```php
use Movex;

class Movex
{
    public function index(){
        $parameters = array('ITNO' => '1234', 'WHLO' => 'A12', 'CONO' => '000');
        Movex::execute('GetItmWhsBasic', 'itemToolboxInterface', $parameters);
    }
}
```

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.


## Security

If you discover any security related issues, please email author email instead of using the issue tracker.

## Credits

- [Aditya Mundhalia][link-author]

## License

license. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/mundhalia/movex.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/mundhalia/movex.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/mundhalia/movex/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/mundhalia/movex
[link-downloads]: https://packagist.org/packages/mundhalia/movex
[link-travis]: https://travis-ci.org/mundhalia/movex
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/aadityamundhalia

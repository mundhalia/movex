<?php

namespace Mundhalia\Movex;

use Illuminate\Support\Facades\Http;
use XmlParser;
use Illuminate\Support\Str;

class Movex
{
    protected $username;
    protected $password;
    protected $url;
    protected $apiLst;

    public function __construct()
    {
        $this->username  = config('movex.auth.username');
        $this->password  = config('movex.auth.password');
        $this->url       = config('movex.auth.url');
        $this->apiLst    = config('movex.auth.api');
    }

    /**
    * This function returns item data from movex item information.
    *
    * The function returns collection of array, the collection is converted from the xml received from movex.
    * this one takes the item number as input value as a string
    *
    * @param string $type this is the service type eg. get, GetItmWhsBasic, etc. for movex
    * @param array $parameters this is where we add the url parameters.
    *
    * @return collection
    */
    public function execute($type, $api, $parameters)
    {
        if(isset($this->apiLst[$api])){
            $url = $this->urlBuilder($this->url, $this->apiLst[$api], $type, $parameters);
            $response = Http::withBasicAuth($this->username, $this->password)
                            ->get($url);
            return $this->route($type, $response);
        }
        else{
            return "Api Not found in list";
        }
    }

    private function route($type, $response)
    {
        if(strval(strpos($type, 'Get')) == "0"){
            return $this->simplifyGet($response->body());
        }
        elseif(strval(strpos($type, 'Lst')) == "0"){
            return $this->simplifyLst($response->body());
        }
    }

    /**
     * @param $xmlObject
     * @param array $out
     * @return array
    */
    private function simplifyGet($response)
    {
        $data = json_decode(json_encode(simplexml_load_string($response)), true);
        $result = array();
        if(isset($data['MIRecord'])){
            foreach ($data['MIRecord'] as $key => $rows) {
                if($key == 'NameValue'){
                    foreach($rows as $row){
                        if(isset($row['Name'])){
                            $result[$row['Name']] = (is_array($row['Value']) ? '' : trim($row['Value']));
                        }
                    }
                }
            }
        }
        return collect([$result]);
    }

    private function simplifyLst($response)
    {
        $data = json_decode(json_encode(simplexml_load_string($response)), true);
        $result = array();
        if(isset($data['MIRecord'])){
            foreach ($data['MIRecord'] as $key => $rows) {
                $collect = array();
                foreach($rows as $k => $v){
                    if($k == 'NameValue'){
                        foreach ($v as $row) {
                            if(isset($row['Name'])){
                                $collect[$row['Name']] = (is_array($row['Value']) ? '' : trim($row['Value']));
                            }
                        }
                    }
                }
                $result[] = $collect;
            }
        }
        return collect($result);
    }

    private function urlBuilder($url, $api, $type, $parameters){
        $url = $this->addSlash($url).$api;
        $url = $this->addSlash($url).$type;

        if(count($parameters) > 0){
            $url .= "?";
            foreach ($parameters as $key => $value) {
                if(!blank($value)){
                    $url .= "$key=$value&";
                }
            }
            $url = substr($url, 0, -1);
        }
        return $url;
    }

    private function addSlash($url)
    {
        if(substr($url, -1) != '/'){
            $url .= '/';
        }
        return $url;
    }
}

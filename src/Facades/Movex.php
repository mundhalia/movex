<?php

namespace Mundhalia\Movex\Facades;

use Illuminate\Support\Facades\Facade;

class Movex extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'movex';
    }
}

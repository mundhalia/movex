<?php

return [
    'auth' => [
        'username' => env('MOVEX_USERNAME'),
        'password' => env('MOVEX_PASSWORD'),
        'url'      => env('MOVEX_URL'),
        'api'      => json_decode(env('MOVEX_API'), true),
    ],
];
